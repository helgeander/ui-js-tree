const uitree = Ractive.extend({
	isolated: true,
	template: '<div class="ui-tree"></div>',
	oncomplete: function() {
		var el = this.find('div');
		var tree = new UiJsTree(
						this.get('tree-data'),
						el,
						this.get('tree-options'));

    	tree.render();
	}
});