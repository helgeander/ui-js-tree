const instance = Ractive({
  data: {
    treeData: [{
      title: 'root',
      children: [{
          title: 'child1',
          collapsed: true,
          children: [{
              title: 'grandchild1-1'
          }, {
              title: 'grandchild1-2'
          }]
      }, {
          title: 'child2',
          children: [{
              title: 'grandchild2-1'
          }, {
              title: 'grandchild2-2'
          }]
      }]
    }],

    treeOptions: {
      initialLevel: 3,
      selectOnKeyEvent: false,
      nodeRenderFn: function(node, el) {
        let span = document.createElement('span');
        let icon = document.createElement('i');
        icon.setAttribute('class', 'material-icons');
        icon.appendChild(document.createTextNode('face'));
        span.appendChild(icon);
        span.appendChild(document.createTextNode(node.title));
        return span;
      },
      onSelect: function(data, el) {
        console.log('onSelect');
      },
      onBeforeSelect: function(data, el) {
        console.log('onBeforeSelect');
      },
      onExpand: function(data, el) {
        console.log('onExpand');
      },
      onBeforeExpand: function(data, el) {
        console.log('onBeforeExpand');
      },
      onCollapse: function(data, el) {
        console.log('onCollapse');
      },
      onBeforeCollapse: function(data, el) {
        console.log('onBeforeCollapse');
      },
      onClick: function(data, el, tree) {
        console.log('onClick');
        console.log(data);
      },
      onKeyPress: function(data, el, tree) {
        console.log('onKeyPress');
        console.log(data);
      }
    }
  },
  target: 'body',
  components: { uitree },
  template: '<uitree tree-data="{{treeData}}" /> <uitree tree-data="{{treeData}}" tree-options="{{treeOptions}}" />'
});
