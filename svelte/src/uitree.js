import UiTree from './UiTree.html';
import UiJsTree from 'ui-js-tree';

const uitree = new UiTree({
  // `target` is the only required option – the element
  // to render the component to
  target: document.querySelector('.uitree'),

  oncreate () {
    var treeData = this.options.treeData;
    var treeOptions = this.options.treeOptions;
    var tree = new UiJsTree(treeData, )
  }
});