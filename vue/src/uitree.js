import Vue from 'vue'

Vue.directive('uitree', {
  // When the bound element is inserted into the DOM...
  bind: function (el, binding, vnode) {
    var treeOptions = binding.value.options;
    var treeData = binding.value.data;
    var tree = new UiJsTree(treeData, el, treeOptions);

    tree.render();
  }
})