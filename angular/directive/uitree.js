angular.module('ui-js-tree', [])
    .factory('UiJsTree', function() {
        return window.UiJsTree;
    })
    .directive('uiJsTreeView', function(UiJsTree, $document) {
        return {
            restrict: 'E',
            replace: true,
            scope: {
                treeData: '<',
                treeOptions: '<?',
                onSelect: '&?'
            },
            template: '<div></div>',
            compile: function(tElem, tAttrs) {
                //Manipulate template element
                return {
                  pre: function(scope, iElem, iAttrs){
                    //logic for finished compiled html elements, but before linking scope
                    scope.tree.clear();
                    scope.tree.render();
                  },
                  post: function(scope, iElem, iAttrs){
                    //logic after linking scope to el
                    //scope.tree.onSelect = scope.onSelect();
                  }
                }
            },
            controller: function($scope, $element) {
                $scope.tree = new UiJsTree(
                    $scope.treeData,
                    $element[0],
                    $scope.treeOptions);

                $scope.$on('selectNode', function(ev, index) {
                    $scope.tree.selectBy(function(node) {
                        return node.data.nodeId == 'tree_view_node_' + index;
                    }, true, true);
                });
                
                $scope.$on('select', function(ev, title) {
                    var data; 
                    $scope.tree.traverse(function(nodeData, parentData) {
                       if (nodeData.title == title)
                         data = nodeData;
                    });
                    $scope.tree.selectByData(data, true, true);
                });

                $scope.$on('reload', function(ev) {
                    $scope.tree.clear();
                    $scope.tree.render();
                });

                $scope.$on('expandAll', function(ev, index) {
                    $scope.tree.expandAll();
                });

                $scope.$on('collapseAll', function(ev, index) {
                    $scope.tree.collapseAll();
                });

                $scope.$on('filter', function(ev, filter, limit) {
                    $scope.tree.filter(filter, limit);
                });

                $scope.$watch('treeData', function(newData, oldData) {
                    if (JSON.stringify(newData) != JSON.stringify(oldData)) {
                        $scope.tree.load(newData);
                    }
                });
            }
        };
    });