angular.module('app', ['ui-js-tree'])
    .controller('appCtrl', function($scope, $locale, $document, $timeout) {
        var lots = [];
        for (var i=1; i<=1000; i++)
            lots[i-1] = { title: 'superchild' + i };
        var lots2 = [];
        for (var i=1; i<=1000; i++)
            lots2[i-1] = { title: 'superchild' + i };

        var treeData = {
            title: 'root',
            children: [{
                title: 'child1',
                children: [{
                    title: 'grandchild1-1'
                }, {
                    title: 'grandchild1-2'
                }]
            }, {
                title: 'child2',
                children: [{
                    title: 'grandchild2-1'
                }, {
                    title: 'grandchild2-2',
                    children: lots
                }]
            }]
        };

        var treeData2 = {
            title: 'things',
            children: [{
                title: 'lots',
                children: lots2
            }]
        };

        $scope.treeData = [treeData, treeData2];
        $scope.treeOptions = {
            lazyRender: true,
            initialLevel: 1,
            nodeRenderFn: function(data, el) {
                return data.title;
            },
            onSelect: function(data, element) {
                $timeout(function() {
                    $scope.nodeContent = data.title + '(' + data.nodeId + ')';
                });
            }
        };
        $scope.nodeContent = 'test';

        $scope.onTreeNodeSelect = function(data, el) {
            $timeout(function() {
                $scope.nodeContent = data.title + '(' + data.nodeId + ')';
            });
        };

        $scope.renderTitle = function(data) {
            return data.title;
        };

        $scope.reload = function() {
            $scope.$broadcast('reload');
        };
        $scope.selectNode = function(index) {
            $scope.$broadcast('selectNode', index);
        };
        $scope.selectNode = function(title) {
            $scope.$broadcast('select', title);
        };
        $scope.expandAll = function() {
            $scope.$broadcast('expandAll');
        };
        $scope.collapseAll = function() {
            $scope.$broadcast('collapseAll');
        };
        $scope.filter = function(filter) {
            if (!$scope.filterValue)
                $scope.reload();
            else
                $scope.$broadcast('filter', function(data) {
                    return data.title.substr(0, $scope.filterValue.length).toLowerCase() == $scope.filterValue.toString().toLowerCase();
                }, 10);
        };

        $scope.$watch('filterValue', function(newVal, oldVal) {
            if (newVal != oldVal)
                $scope.filter(newVal);
        });
    });