describe('ui-js-tree render', function() {

  it('contains focus element', function() {
    cy.get('a.tree-focus-el').should('exist')
  })

  it('should contain root node element', function() {
    let nodeText = cy.get('div.ui-tree > a > ul > li > span')
    nodeText.contains('root')
    nodeText.click()
    cy.get('div.ui-tree > a > ul > li').should('have.class', 'selected')
  })

  it('should add new nodes with tree.addToNode(newData, node)', () => {
    cy.get('div.ui-tree > a > ul > li').then(($el) => {
      cy.window().its('tree').invoke('getNodeData', $el[0]).then(nodeData => {
        cy.window().its('tree').invoke('addToNode', [{title: 'newChild1'},{title: 'newChild2'}], nodeData)
        cy.contains('newChild1')
        cy.contains('newChild2')
      })
    })
  })

})

describe('ui-js-tree select next and prev', function() {
  it('should invoke tree.nextNode()', () => {
    cy.window().its('tree').invoke('firstNode')
    cy.window().its('tree.selected.title').should('eq', 'root')
    cy.window().its('tree').invoke('nextNode')
    cy.window().its('tree.selected.title').should('eq', 'child')
  })

  it('should invoke tree.prevNode()', () => {
    cy.window().its('tree').invoke('lastNode')
    cy.window().its('tree').invoke('prevNode')
    cy.window().its('tree.selected.title').should('eq', 'grandchild2-1')
  })
})

describe('ui-js-tree keydown', function() {
  it('should select next node on keydown with key=down', function() {
    cy.window().its('tree').invoke('firstNode')
    cy.get('a.tree-focus-el').type('{downarrow}')
    cy.get('div.ui-tree > a > ul > li > ul > li').should('have.class', 'selected')
    cy.window().its('tree.selected.title').should('eq', 'child')
    cy.get('a.tree-focus-el').type('{uparrow}')
    cy.window().its('tree.selected.title').should('eq', 'root')
  })
})

describe('ui-js-tree expand and collapse', function() {

  it('should collapse and expand node on click', function() {
    let node = cy.get('div.ui-tree > a > ul > li')
    node.should('not.have.class', 'collapsed')

    node.trigger('click', { position: 'topLeft' })
    node.should('have.class', 'collapsed')
    node.find('ul').first().should('not.be.visible')

    node = cy.get('div.ui-tree > a > ul > li')
    node.trigger('click', { position: 'topLeft' })
    node.should('not.have.class', 'collapsed')
  })
})

describe('ui-js-tree lazy expand', function() {
  before(function() {
    cy.visit('http://localhost:8080')
    cy.fixture('default.config.json').then(data => {
      cy.window().invoke('initTree', data, { lazyRender: true, initialLevel: 1 });
    })
  })

  it('should collapse and expand node on click', function() {
    let node = cy.get('div.ui-tree > a > ul > li')
    node.should('have.class', 'collapsed')
    node.find('ul').should('not.exist')

    node = cy.get('div.ui-tree > a > ul > li')
    node.trigger('click', { position: 'topLeft' })
    node.should('not.have.class', 'collapsed')
    node.find('ul').should('exist')
  })
})

describe('ui-js-tree event callbacks', function() {
  before(function() {
    cy.visit('http://localhost:8080')
    cy.fixture('default.config.json').then(data => {
      cy.window().then(win => {
        win.initTree(data, {
          initialLevel: 1,
          onSelect: function(data, el) {
            win.onSelectCalled = true;
          },
          onBeforeSelect: function(data, el) {
            win.onBeforeSelectCalled = true;
          },
          onExpand: function(data, el) {
            win.onExpandCalled = true;
          },
          onBeforeExpand: function(data, el) {
            win.onBeforeExpandCalled = true;
          },
          onCollapse: function(data, el) {
            win.onCollapseCalled = true;
          },
          onBeforeCollapse: function(data, el) {
            win.onBeforeCollapseCalled = true;
          }
        })
      })
    })
  })

  it('should respond to events', function() {
    // cy.window().invoke('tree.onSelect')
    cy.window().its('tree').invoke('firstNode')
    cy.window().its('onSelectCalled').should('eq', true)
    cy.window().its('onBeforeSelectCalled').should('eq', true)

    let node = cy.get('div.ui-tree > a > ul > li')
    node.trigger('click', { position: 'topLeft' })
    cy.window().its('onExpandCalled').should('eq', true)
    cy.window().its('onBeforeExpandCalled').should('eq', true)
    cy.window().then(win => { expect(win.onCollapseCalled, 'onCollapseCalled').to.be.undefined })
    cy.window().then(win => { expect(win.onBeforeCollapseCalled, 'onBeforeCollapseCalled').to.be.undefined })

    node = cy.get('div.ui-tree > a > ul > li')
    node.trigger('click', { position: 'topLeft' })
    cy.window().its('onCollapseCalled').should('eq', true)
    cy.window().its('onBeforeCollapseCalled').should('eq', true)
  })
})